package io.gainable.transactions.service;

import io.gainable.transactions.domain.Transaction;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class TransactionService {
    private List<Transaction> transactions = Stream.of(
            new Transaction("some type", "1", "EUR", 1.0, "Schwarz KG", "®"),
            new Transaction("other type", "2", "USD", 1.1, "Walmart Inc.", "®"),
            new Transaction("other type", "3", "AUD", 1.5, "Coles Supermarkets Australia Pty Ltd", "®"),
            new Transaction("other type", "3", "AUD", 2.5, "Coles Supermarkets Australia Pty Ltd", "®"),
            new Transaction("other type", "4", "AUD", 0.5, "Woolworths Group Limited", "®")
    ).collect(Collectors.toList());
    public List<Transaction> findAllByAccountNumber(String accountNumber) {
        return transactions.stream()
                .filter(t -> t.getAccountNumber().equalsIgnoreCase(accountNumber))
                .collect(Collectors.toList());
    }
}
