package io.gainable.transactions.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Transaction {
    private String type;
    private String accountNumber;
    private String currency;
    private Double amount;
    private String merchantName;
    private String merchantLogo;
}
