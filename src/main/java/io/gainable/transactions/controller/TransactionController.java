package io.gainable.transactions.controller;

import io.gainable.transactions.domain.Transaction;
import io.gainable.transactions.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/transactions", produces = "application/json")
public class TransactionController {
    Logger logger = LoggerFactory.getLogger(TransactionController.class);

    private TransactionService transactionService;

    TransactionController() {
        transactionService = new TransactionService();
    }

    @GetMapping(path = "/{accountNumber}")
    public Iterable<Transaction> getTransactions(@PathVariable("accountNumber") String accountNumber) {
        logger.debug("account number:" + accountNumber);
        return transactionService.findAllByAccountNumber(accountNumber);
    }
}
