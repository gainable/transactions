package io.gainable.transactions.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TransactionControllerIntegrationTest {
    @Test
    void getATransactions(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(get("/transactions/a"))
                .andExpect(status().isOk());
    }

    @Test
    void getTransactions(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(get("/transactions/1"))
                .andExpect(status().isOk());
    }
}