package io.gainable.transactions.component;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.get;
import static org.hamcrest.Matchers.hasItems;

public class TransactionComponentTest {
    @Test
    public void blackBoxTest() {
        get("/transactions/3")
                .then().body(
                "type", hasItems("other type", "other type"),
                "accountNumber", hasItems("3", "3"),
                "currency", hasItems("AUD", "AUD"),
                "amount", hasItems(1.5f, 2.5f),
                "merchantName", hasItems("Coles Supermarkets Australia Pty Ltd", "Coles Supermarkets Australia Pty Ltd"),
                "merchantLogo", hasItems("®", "®")
        );
    }
}
