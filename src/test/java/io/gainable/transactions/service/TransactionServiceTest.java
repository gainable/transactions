package io.gainable.transactions.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class TransactionServiceTest {

    @Autowired
    private TransactionService transactionService;

    @Test
    void findAllByAccountNumber() {
        assertThat(transactionService.findAllByAccountNumber("1")).hasSize(1);
    }
}